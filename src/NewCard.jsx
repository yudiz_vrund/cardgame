import React, { useEffect, useState } from 'react';
import './card.css';

const NewCard = () => {

    const [open, setOpen] = useState([]);

    const [matched, setMatched] = useState([]);

    const [counter,setCounter] = useState(0);

    const cardData = [
        {
            id: 'amz',
            img: "https://i.pinimg.com/originals/08/5f/d8/085fd8f7819dee3b716da73d3b2de61c.jpg",
        },
        {
            id: 'myntra',
            img: "https://images.indianexpress.com/2021/01/myntra.png",
        },
        {
            id: 'yudiz',
            img: "https://media.glassdoor.com/sqll/794070/yudiz-solutions-squarelogo-1431416437293.png",
        },
        {
            id: 'delhicapitals',
            img: "https://upload.wikimedia.org/wikipedia/en/thumb/f/f5/Delhi_Capitals_Logo.svg/1200px-Delhi_Capitals_Logo.svg.png",
        },
        {
            id: 'punjab',
            img: "https://upload.wikimedia.org/wikipedia/en/1/1c/Punjab_Kings_logo_2021.png",
        },
        {
            id: 'mumbai',
            img: "https://i.pinimg.com/originals/28/09/a8/2809a841bb08827603ccac5c6aee8b33.png",
        },
        {
            id: 'chennai',
            img: "https://upload.wikimedia.org/wikipedia/en/thumb/2/2b/Chennai_Super_Kings_Logo.svg/1200px-Chennai_Super_Kings_Logo.svg.png",
        },
        {
            id: 'rajasthan',
            img: "https://upload.wikimedia.org/wikipedia/en/thumb/6/60/Rajasthan_Royals_Logo.svg/1200px-Rajasthan_Royals_Logo.svg.png",
        },
        {
            id: 'kkr',
            img: "https://upload.wikimedia.org/wikipedia/en/thumb/4/4c/Kolkata_Knight_Riders_Logo.svg/1200px-Kolkata_Knight_Riders_Logo.svg.png",
        },
        {
            id: 'who',
            img: "https://www.who.int/images/default-source/infographics/who-emblem.png?sfvrsn=877bb56a_2",
        },
        {
            id: 'microsoft',
            img: "https://cdn.vox-cdn.com/thumbor/NeSo4JAqv-fFJCIhb5K5eBqvXG4=/7x0:633x417/1200x800/filters:focal(7x0:633x417)/cdn.vox-cdn.com/assets/1311169/mslogo.jpg",
        },
        {
            id: 'google',
            img: "https://res.cloudinary.com/practicaldev/image/fetch/s--ZUMyUgWZ--/c_imagga_scale,f_auto,fl_progressive,h_1080,q_auto,w_1080/https://dev-to-uploads.s3.amazonaws.com/i/am6lv2x37bole6x4poz3.jpg",
        },
        {
            id: 'barclays',
            img: "https://1000logos.net/wp-content/uploads/2016/10/Barclays-logo.png",
        },
        {
            id: 'mindbody',
            img: "https://i.pinimg.com/originals/f2/86/a7/f286a7a68e80971acba65963e5e12b57.png",
        },
        {
            id: 'heart',
            img: "https://i.pinimg.com/originals/2c/91/a8/2c91a84269260953e662320dd798796e.png",
        },
    ];
    const cards = [...cardData, ...cardData];
    //const ShuffleCards = cards.sort(()=>Math.random() - 0.5);


    const flipCard = index => {
        setOpen((opened) => [...opened, index]);
    }

    useEffect(() => {

        const firstCard = cards[open[0]];
        const secondCard = cards[open[1]];

        if (open.length > 1) {
            if (firstCard.id === secondCard.id) {
                setMatched([...matched, firstCard.id]);
                setCounter(counter+1);
            }
        }

        if (open.length === 2) {
            setTimeout(() => setOpen([]), 1000);
        }

        

    }, [open]);



    return (
        <div className="cards">
            
            {cards.map((data, index) => {
                let isFlipped = false;

                if (open.includes(index)) {
                    isFlipped = true;
                }
                if (matched.includes(data.id)) {
                    isFlipped = true;
                }


                return (
                    <div
                        className={`data-card ${isFlipped ? "flipped" : ""} `}
                        key={index}
                        onClick={() => flipCard(index)}
                    >
                        <div className="inner">
                            <div className="front">
                                <img
                                    src={data.img} alt="data" width="70" height="60"
                                />
                            </div>
                            <div className="back"></div>
                        </div>

                    </div>

                );
            })}
        <span>Counter is : {counter}</span>
        </div>


    )

}

export default NewCard